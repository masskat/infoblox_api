# infoblox_api
Simple Python 3 Infoblox API library. Supports both NIOS and CSP API.

Classes:

* **NIOSclient**: NIOS API client

* **CSPclient**: CSP API client

* **CSPimport**: Set of tools used to import data to CSP; currently supports DHCP and DNS (A, AAAA, CNAME, MX, PTR, TXT, SRV records) configuration import from NIOS CVS files

* **ibResponse**: Handles API responses

* **ibException**: Exceptions handling for the whole library

## Requirements

Library requires **ndict** package. It can be installed in the following way:
```
pip install git+https://bitbucket.org/masskat/nested_dict
```

## Installation

### Cloning the repository:
```
git clone https://bitbucket.org/masskat/infoblox_api.git
```

### Installing directly from the repository using pip
```
pip install git+https://bitbucket.org/masskat/infoblox_api
```

### Upgrading directly from the repository using pip
```
pip install --upgrade git+https://bitbucket.org/masskat/infoblox_api
```

### Local installation with pip
Download the archive, unpack it, navigate to the repository directory and run:
```
pip install .
```

### Uninstalling
```
pip uninstall ibapi
```

## Development Environment
The library was developed using Python 3.8.
