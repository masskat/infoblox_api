import json
from ipaddress import IPv4Network
from ndict import NestedDict
from .ibapi_exceptions import ibException



class CSPimport:
    """
    Import data to CSP from NIOS CSV files. Input files should have ';' delimeter. CSV headers are not case sensitive.

    Currently following CSV file types are supported:

    Networks - options should be defined as additional columns in 'OPTION-<number>' format.
      header-network;address*;netmask*;comment;dhcp_members;domain_name;domain_name_servers;lease_time;routers;OPTION-4;OPTION-42;OPTION-66;OPTION-67
      network;192.168.9.0;255.255.255.0;Vlan 1;infoblox.localdomain;mh.local;192.168.9.5,192.168.9.4;345600;192.168.9.1;;;;
      network;192.168.12.0;255.255.255.0;Vlan 2;infoblox.localdomain;;10.50.50.37,10.50.50.38;345600;192.168.12.1;;;;
      network;192.168.52.0;255.255.255.0;Vlan 3;infoblox.localdomain;mh.local;10.50.50.37,10.50.50.38;691200;192.168.52.1;192.168.52.1;;;

    Network ranges
      header-dhcprange;end_address*;start_address*;exclusion_ranges;comment
      dhcprange;192.168.9.200;192.168.9.150;;
      dhcprange;192.168.12.254;192.168.12.10;;
      dhcprange;192.168.52.254;192.168.52.10;192.168.52.11-192.168.52.41,192.168.52.81-192.168.52.240;

    Fixed IP addresses - 'mac_address' = <MAC address>, 'match_option' = 'MAC_ADDRESS'
    Reserved IP address - 'mac_address' = '00:00:00:00:00:00', 'match_option' = 'RESERVED'
      header-fixedaddress;ip_address*;mac_address;comment;name;match_option
      fixedaddress;192.168.9.154;00:11:0a:f9:94:6b;;host1.mh.local;MAC_ADDRESS
      fixedaddress;192.168.9.155;00:12:3f:55:50:c9;Test;host2.mh.local;MAC_ADDRESS
      fixedaddress;192.168.9.156;5c:b9:01:12:04:d6;;host3.mh.local;MAC_ADDRESS
      fixedaddress;192.168.52.10;00:25:64:96:3c:ae;;host4.mh.local;MAC_ADDRESS
      fixedaddress;192.168.52.43;00:90:e8:86:82:f7;;host5.mh.local;MAC_ADDRESS
      fixedaddress;192.168.52.9;00:00:00:00:00:00;;host6.mh.local;RESERVED

    DNS A records (all headers below are mandatory except 'comment')
      Header-ARecord;fqdn*;address*;ttl;comment
      ARecord;raz.lab.local;192.168.38.12;;Test 1
      ARecord;lab.local;192.168.38.11;600;Test 2

    DNS AAAA record (all headers below are mandatory except 'comment')
      Header-ARecord;fqdn*;address*;ttl;comment
      AaaaRecord;lab.local;2001:db8:85a3:8d3:1319:8a2e:370:7348;600;Test 2

    DNS CNAME record (all headers below are mandatory except 'comment')
      Header-CnameRecord;fqdn*;canonical_name*;ttl;comment
      CnameRecord;test1.lab.local;test.lab.local;28800;

    DNS MX record (all headers below are mandatory except 'comment')
      Header-MxRecord;fqdn*;mx*;priority*;ttl;comment
      MxRecord;mail.lab.local;test.lab.local;10;28800;

    DNS PTR record (all headers below are mandatory except 'comment')
      Header-PtrRecord;fqdn*;dname*;ttl;comment
      PtrRecord;20.10.168.192.in-addr.arpa;rev20.lab.local;28800;

    DNS TXT record (all headers below are mandatory except 'comment')
      Header-TxtRecord;fqdn*;text*;ttl;comment
      TxtRecord;txt.lab.local;text;600;Test TXT record

    DNS SRV record (all headers below are mandatory except 'comment')
      Header-SrvRecord;fqdn*;priority*;weight*;port*;target*;ttl;comment
      SrvRecord;_kerberos._tcp.lab.local;0;100;88;win2016-dc2.lab.local;600;
      SrvRecord;_kerberos._tcp.lab.local;0;100;88;win2016-dc1.lab.local;600;
    """


    def __init__(self, csp_client):
        """
        Constructor.

        :param csp_client: CSPclient object.
        """
        self.csp_client = csp_client



### Generic methods ###


    @staticmethod
    def check_csv(csv_dict):
        """
        Verify CSV dictionary and get details from it.

        Information about checked fields in CSV files is defined in 'check_def' dictionary.
        'check_def' keys (e.g. 'header-network') represent the first column of a CSV file. This column defines an entry type (.e.g. 'network', 'dhcprange').
        For each type following parameters are defined:
            - 'req_headers': list of required headers,
            - 'allowed_types': list of allowed types of entries in the first column of the CSV file.
            - 'item_id': list used to construct item id (e.g. <subnet>/<mask>); normally used for logging.

        For each line a following dictionary is returned:
            {
                'ok': True/False,                           # CSV line is correctly recognized?
                'header_type': <recognized type header>,    # e.g. 'header-network'
                'type': <recognized item type>,             # e.g. 'network', 'dhcprange'
                'item_id': <specific item id>               # e.g. '192.168.1.0/255.255.255.0'
            }

        :param csv_dict: Dictionary representing single CSV line - {<header>: <value>}. Headers are not case sensitive.
        :return: {'ok': True/False, 'header_type': <recognized type header>, 'type': <recognized item type>, 'item_id': <specific item id>}
                 NOTE: 'header_type' is always returned in lower case.
        """

        # CVS dict. definitions.
        check_def = {
            'header-network': {
                'req_headers': ['address*', 'netmask*', 'comment', 'domain_name', 'domain_name_servers', 'lease_time', 'routers'],
                'allowed_types': ['network'],
                'item_id': ['address*', '/', 'netmask*']
            },
            'header-dhcprange': {
                'req_headers': ['start_address*', 'end_address*', 'exclusion_ranges', 'comment'],
                'allowed_types': ['dhcprange'],
                'item_id': ['start_address*', '-', 'end_address*']
            },
            'header-fixedaddress': {
                'req_headers': ['ip_address*', 'mac_address', 'match_option', 'name', 'comment'],
                'allowed_types': ['fixedaddress'],
                'item_id': ['ip_address*']
            },
            'header-arecord':{
                'req_headers': ['fqdn*', 'address*', 'ttl'],
                'allowed_types': ['arecord'],
                'item_id': ['fqdn*']
            },
            'header-aaaarecord':{
                'req_headers': ['fqdn*', 'address*', 'ttl'],
                'allowed_types': ['aaaarecord'],
                'item_id': ['fqdn*']
            },
            'header-cnamerecord':{
                'req_headers': ['fqdn*', 'canonical_name*', 'ttl'],
                'allowed_types': ['cnamerecord'],
                'item_id': ['fqdn*']
            },
            'header-mxrecord':{
                'req_headers': ['fqdn*', 'mx*', 'priority*', 'ttl'],
                'allowed_types': ['mxrecord'],
                'item_id': ['fqdn*']
            },
            'header-ptrrecord':{
                'req_headers': ['fqdn*', 'dname*', 'ttl'],
                'allowed_types': ['ptrrecord'],
                'item_id': ['fqdn*']
            },
            'header-txtrecord':{
                'req_headers': ['fqdn*', 'text*', 'ttl'],
                'allowed_types': ['txtrecord'],
                'item_id': ['fqdn*']
            },
            'header-srvrecord':{
                'req_headers': ['fqdn*', 'priority*', 'weight*', 'port*', 'target*', 'ttl'],
                'allowed_types': ['srvrecord'],
                'item_id': ['fqdn*']
            }
        }

        # CSV dict with lowecase keys.
        csv_dict_lk = {k.lower(): v for k, v in csv_dict.items()}

        # Check if CSV dict is valid
        ok = False
        used_header_type = ''
        used_type = ''
        for header_type in check_def:
            if header_type in csv_dict_lk:
                ok = True
                used_header_type = header_type
                for header in check_def[header_type]['req_headers']:
                    if not header in csv_dict_lk:
                        ok = False
                if not csv_dict_lk[header_type].lower() in check_def[header_type]['allowed_types']:
                    ok = False
                else:
                    used_type = csv_dict_lk[header_type]

        # Construct item id (e.g. <subnet>/<mask>).
        item_id = ''
        if ok:
            for item_id_element in check_def[used_header_type]['item_id']:
                if item_id_element in csv_dict_lk:
                    item_id += csv_dict_lk[item_id_element]
                else:
                    item_id += item_id_element

        # If 'header_type' or 'type' is not recognized, value is ''. 'item_id' is set only if CSV dict is ok.
        return {'ok': ok, 'header_type': used_header_type, 'type': used_type, 'item_id': item_id}


    def generic_obj_dict(self, resource, param_list):
        """
        Get generic object dictionary organized by values of specified params.

        Let's consider following JSON response:
            {
                "results": [
                    {
                        "code": 2,
                        "name": "time-offset",
                        "option_space": "space1"
                    },
                    {
                        "code": 3,
                        "name": "routers",
                        "option_space": "space1"
                    },
                    {
                        "code": 4,
                        "name": "WebServerPort",
                        "option_space": "space2"
                    }
                ]
            }

        When the 'param_list' argument is defined as ['option_space', 'code'], method will return following dictionary:
            {
                "space1": {
                    "2": {
                        "code": 2,
                        "name": "time-offset",
                        "option_space": "space1"
                    },
                    "3": {
                        "code": 3,
                        "name": "routers",
                        "option_space": "space1"
                    }
                },
                "space2": {
                    "4": {
                        "code": 4,
                        "name": "WebServerPort",
                        "option_space": "space2"
                    }
                }
            }

        :param resource: Accessed resource (e.g. '/dhcp/option_code').
        :param param_list: List or single string used to organize result, e.g. ['param1', 'param2'].
                           Note that the rightmost param value MUST be unique, otherwise some returned objects will be lost.
        :return: Dictionary with objects organized based on 'param_list', e.g. {'val1': {'val2': {<object_definition>}}}
        """

        # API call.
        api_res = self.csp_client.api_call(resource=resource)

        # When a single param is passed, convert it to list.
        if isinstance(param_list, str):
            param_list = [param_list]

        # Response successful - return processed data.
        if api_res.success:
            generic_ndict = NestedDict()
            for generic_item in api_res.json()['results']:
                generic_ndict[tuple([generic_item[param] for param in param_list])] = generic_item
            return generic_ndict.dict

        # Response unsuccessful - raise an exception.
        else:
            raise ibException('Failed to obtain data from resource ' + resource + '. Check your API key and user permissions.')



### DHCP methods ###


    def option_dict(self):
        """
        Return options dictionary organized by option space name and option code.

        Dictionary is returned in following format:
            {
                "<option space name>": {
                    "<option code as string>": {<single option dictionary>}
                }
            }

        :return: Option dictionary in {"<option space name>": {"<option code as string>": {<single option dictionary>}}} format.
        """
        options = self.generic_obj_dict('/dhcp/option_code', ['option_space', 'code'])
        opt_spaces = self.generic_obj_dict('/dhcp/option_space', 'id')
        return {opt_spaces[space_id]['name']: options[space_id] for space_id in options}


    def option_id(self, code, option_space='dhcp4'):
        """
        Get option id.

        :param code: Option code (integer).
        :param option_space: (optional) Option space name ('dhcp4' by default).
        :return: Option id (e.g. "dhcp/option_code/14bc5011-626b-11eb-8727-029388612d3e") or raise an exception if name not found.
        """
        try:
            return self.option_dict()[option_space][code]['id']
        except KeyError:
            raise ibException('Could not find option code ' + str(code) + ' in option space ' + option_space + '.')


    def dhcp_host_dict(self):
        """
        Returns DHCP hosts dictionary organized by their names.

        Dictionary is returned in the following format:
            {
                "<DHCP host name>": {<single DHCP object dictionary>}
            }

        :return: Hosts dictionary.
        """
        return self.generic_obj_dict('/dhcp/host', 'name')


    def dhcp_host_id(self, name):
        """
        DHCP host id.

        :param name: DHCP host name.
        :return: DHCP host id (e.g. 'dhcp/host/213611') or raise an exception if name not found.
        """
        try:
            return self.dhcp_host_dict()[name]['id']
        except KeyError:
            raise ibException(name + ' object not found.')


    def ha_group_dict(self):
        """
        Returns HA groups dictionary organized by their names.

        Dictionary is returned in the following format:
            {
                "<HA group name>": {<single HA group object dictionary>}
            }

        :return: HA groups dictionary.
        """
        return self.generic_obj_dict('/dhcp/ha_group', 'name')


    def ha_group_id(self, name):
        """
        HA group id.

        :param name: HA group name.
        :return: HA group id or raise an excpeption if name not found.
        """
        try:
            return self.generic_obj_dict('/dhcp/ha_group', 'name')[name]['id']
        except KeyError:
            raise ibException(name + ' object not found.')


    def ip_space_dict(self):
        """
        Return IP spaces dictionary organized by names.

        Dictionary is returned in the following format:
            {
                "<IP space name>": {<single IP space object dictionary>}
            }

        :return: IP spaces dictionary.
        """
        return self.generic_obj_dict('/ipam/ip_space', 'name')


    def ip_space_id(self, name):
        """
        IP space id.

        :param name: IP space name.
        :return: IP space id (e.g. 'ipam/ip_space/0f9d84b2-7a93-11eb-9e84-bad43570c956') or raise an exception if name not found.
        """
        try:
            return self.generic_obj_dict('/ipam/ip_space', 'name')[name]['id']
        except KeyError:
            raise ibException(name + ' object not found.')


    def post_network_csv(self, csv_dict, space_id, options_dict={}, dst_id='', force=False):
        """
        POST new single network from NIOS CSV file.

        :param csv_dict: Dictionary containing a single line of CSV file in {<header>: <value>} format.
        :param space_id: IP space id as defined in CSP (e.g. taken from the ip_space_id() method).
        :param options_dict: (optional) Dictionary containing definitions of all options defined in CSP in in {"<option space name>": {"<option code as string>": {<single option dictionary>}}} format.
                             Usually provided by the option_dict() method.
        :param dst_id: (optional) Id of a host or HA group to which specific network should be assigned. If not specified (default), added network is not assigned to any host or HA group.
        :param force: (optional) True/False. Proceed even if options from the CSV file are not defined in CSP ('options_dict'). In such case unknown options will be omitted.
                      If param. is False (default) an ibException exception will be raised.
        :return: ibResponse object.
        """

        # Check CSV format and raise an exception if it is incorrect.
        check = self.check_csv(csv_dict)
        if not check['ok']:
            raise ibException('Incorrect CSV file format for post_network_csv() in:\n' + str(csv_dict))
        elif not check['header_type'] == 'header-network':
            raise ibException('Incorrect header type for post_network_csv() in:\n' + str(csv_dict))

        # CSV dict with lowecase keys.
        csv_dict_lk = {k.lower(): v for k, v in csv_dict.items()}

        # Default options found in CSV file.
        csv_options = {
            'domain_name': ('dhcp4', 15),
            'domain_name_servers': ('dhcp4', 6),
            'routers': ('dhcp4', 3)
        }

        # Get custom options from CSV line.
        for key in csv_dict_lk:
            if key.find('OPTION') == 0:
                opt_list = key[7:].rsplit('-', 1)
                if len(opt_list) == 1:
                    opt_list.insert(0, 'dhcp4')
                    opt_list[1] = int(opt_list[1])
                csv_options[key] = tuple(opt_list)

        # Final list of options to be attached to the POST JSON body.
        post_options = []
        options_ndict = NestedDict(options_dict)
        for opt in csv_options:
            if csv_options[opt] in options_ndict:
                if csv_dict_lk[opt]:
                    post_options.append({'option_code': options_ndict[csv_options[opt]]['id'], 'option_value': csv_dict_lk[opt], 'type': 'option'})
            else:
                if not force:
                    raise ibException('Option ' + opt + ' not defined in CSP.')

        # Construct JSON body of the message.
        body = {
            'address': IPv4Network(csv_dict_lk['address*'] + '/' + csv_dict_lk['netmask*']).with_prefixlen,
            'space': space_id,
            'comment': csv_dict_lk['comment'],
        }
        if dst_id:
            body['dhcp_host'] = dst_id
        if post_options:
            body['dhcp_options'] = post_options
        if csv_dict_lk['lease_time']:
            body['dhcp_config'] = {'lease_time': int(csv_dict_lk['lease_time'])}

        # Return ibResponse from the API call.
        return self.csp_client.api_call(method='post', resource='/ipam/subnet', data=json.dumps(body))


    def post_range_csv(self, csv_dict, space_id):
        """
        POST new single DHCP range from NIOS CSV file.

        :param csv_dict: Dictionary containing a single line of CSV file in {<header>: <value>} format.
        :param space_id: IP space id as defined in CSP (e.g. taken from the ip_space_id() method).
        :return: ibResponse object.
        """

        # Check CSV format and raise an exception if it is incorrect.
        check = self.check_csv(csv_dict)
        if not check['ok']:
            raise ibException('Incorrect CSV file format for post_range_csv() in:\n' + str(csv_dict))
        elif not check['header_type'] == 'header-dhcprange':
            raise ibException('Incorrect header type for post_range_csv() in:\n' + str(csv_dict))

        # CSV dict with lowecase keys.
        csv_dict_lk = {k.lower(): v for k, v in csv_dict.items()}

        body = {
            'start': csv_dict_lk['start_address*'],
            'end': csv_dict_lk['end_address*'],
            'space': space_id,
            'comment': csv_dict_lk['comment']
        }

        # Process exclusion ranges
        if csv_dict_lk['exclusion_ranges']:
            body['exclusion_ranges'] = []
            for ex_range in csv_dict_lk['exclusion_ranges'].split(','):
                ex_range_list = ex_range.split('-')
                body['exclusion_ranges'].append({'start': ex_range_list[0], 'end': ex_range_list[1]})

        # Return ibResponse from the API call.
        return self.csp_client.api_call(method='post', resource='/ipam/range', data=json.dumps(body))


    def post_fixedip_csv(self, csv_dict, space_id):
        """
        POST single fixed or reserved IP from NIOS CSV file.

        :param csv_dict: Dictionary containing a single line of CSV file in {<header>: <value>} format.
        :param space_id: IP space id as defined in CSP (e.g. taken from the ip_space_id() method).
        :return: ibResponse object.
        """

        # Check CSV format and raise an exception if it is incorrect.
        check = self.check_csv(csv_dict)
        if not check['ok']:
            raise ibException('Incorrect CSV file format for post_fixedip_csv() in:\n' + str(csv_dict))
        elif not check['header_type'] == 'header-fixedaddress':
            raise ibException('Incorrect header type for post_fixedip_csv() in:\n' + str(csv_dict))

        # CSV dict with lowecase keys.
        csv_dict_lk = {k.lower(): v for k, v in csv_dict.items()}

        # Common items.
        body = {
            'address': csv_dict_lk['ip_address*'],
            'comment': csv_dict_lk['comment'] if csv_dict_lk['comment'] else csv_dict_lk['name']     # If original comment doesn't exist, replace it with a name.
        }

        # Fixed IP matched by MAC. Add specific params to the body and set resource used in the API call.
        if csv_dict_lk['match_option'] == 'MAC_ADDRESS':
            csp_resource = '/dhcp/fixed_address'
            body['ip_space'] = space_id
            body['match_type'] = 'mac'
            body['match_value'] = csv_dict_lk['mac_address']
            if csv_dict_lk['name']:
                body['name'] = csv_dict_lk['name']

        # IP reservation. Add specific params to the body and set resource used in the API call.
        elif csv_dict_lk['match_option'] == 'RESERVED':
            csp_resource = '/ipam/address'
            body['space'] = space_id
            if csv_dict_lk['name']:
                body['names'] = [{'name': csv_dict_lk['name'], 'type': 'user'}]

        # Not supported match_option value.
        else:
            raise ibException('Not supported match_option value in post_fixedip_csv().')

        # Return ibResponse from the API call.
        return self.csp_client.api_call(method='post', resource=csp_resource, data=json.dumps(body))



### DNS methods ###

    def view_id(self, name):
        """
        DNS view id.

        :param name: View name.
        :return: View id (e.g. 'dns/view/0f9d84b2-7a93-11eb-9e84-bad43570c956') or raise an exception if name not found.
        """
        try:
            return self.generic_obj_dict('/dns/view', 'name')[name]['id']
        except KeyError:
            raise ibException(name + ' object not found.')


    def post_record_csv(self, csv_dict, view_id):
        """
        POST a new single DNS record from a NIOS CSV file.

        :param csv_dict: Dictionary containing a single line of CSV file in {<header>: <value>} format.
        :param view_id: DNS view id as defined in CSP (e.g. taken from the view_id() method).
        :return: ibResponse object.
        """

        # Supported header types and corresponding record types.
        header_types = {
            'header-arecord': 'A',
            'header-aaaarecord': 'AAAA',
            'header-cnamerecord': 'CNAME',
            'header-mxrecord': 'MX',
            'header-ptrrecord': 'PTR',
            'header-txtrecord': 'TXT',
            'header-srvrecord': 'SRV'
        }

        # Check CSV format, find record type and raise an exception if it is incorrect.
        check = self.check_csv(csv_dict)
        if check['ok']:
            if check['header_type'] in header_types:
                rtype = header_types[check['header_type']]
            else:
                raise ibException('Incorrect header type for post_record_csv() in:\n' + str(csv_dict))
        else:
            raise ibException('Incorrect CSV file format for post_record_csv() in:\n' + str(csv_dict))

        # CSV dict with lowecase keys.
        csv_dict_lk = {k.lower(): v for k, v in csv_dict.items()}

        # Record data.
        rdata = {

            # A/AAAA
            'address': csv_dict_lk.get('address*', ''),

            # CNAME
            'cname': csv_dict_lk['canonical_name*'] + '.' if csv_dict_lk.get('canonical_name*') else '',

            # MX
            'exchange': csv_dict_lk['mx*'] + '.' if csv_dict_lk.get('mx*') else '',
            'preference': int(csv_dict_lk['priority*']) if csv_dict_lk.get('priority*', '').isdigit() and rtype == 'MX' else '',

            # PTR
            'dname': csv_dict_lk['dname*'] + '.' if csv_dict_lk.get('dname*') else '',

            # TXT
            'text': csv_dict_lk.get('text*', ''),

            # SRV
            'port': int(csv_dict_lk['port*']) if csv_dict_lk.get('port*', '').isdigit() else '',
            'priority': int(csv_dict_lk['priority*']) if csv_dict_lk.get('priority*', '').isdigit() and rtype == 'SRV' else '',
            'target': csv_dict_lk['target*'] + '.' if csv_dict_lk.get('target*') else '',
            'weight': int(csv_dict_lk['weight*']) if csv_dict_lk.get('weight*', '').isdigit() else ''

        }

        # JSON body used in API call.
        body = {
            'type': rtype,
            'absolute_name_spec': csv_dict_lk['fqdn*'] + '.',
            'view': view_id,
            'ttl': int(csv_dict_lk['ttl']) if csv_dict_lk['ttl'].isdigit() else '',
            'rdata': {k: rdata[k] for k in rdata if not rdata[k] == ''},
            'comment': csv_dict_lk.get('comment', '')
        }

        # Return ibResponse from the API call.
        return self.csp_client.api_call(method='post', resource='/dns/record', data=json.dumps({k: body[k] for k in body if not body[k] == ''}))
