import requests
from urllib3.exceptions import InsecureRequestWarning
from .ibapi_response import ibResponse
from .ibapi_exceptions import ibException


class NIOSclient:
    """
    NIOS API client class.
    """

    def __init__(self, verify_cert=True):
        """
        Constructor.

        :param verify_cert: (optional) Verify certificate.
                            If not specified, certificates are checked.
        """
        self.verify_cert = verify_cert                      # Verify certificate.
        self.url = ''                                       # Base URL.
        self.cookie = requests.cookies.RequestsCookieJar()  # Cookie used to maintain session.


    @staticmethod
    def disable_warnings():
        """
        Disable insecure request warnings.
        """
        requests.packages.urllib3.disable_warnings(category=InsecureRequestWarning)


    def login(self, host='127.0.0.1', api_version='v2.7', login='admin', pwd='infoblox'):
        """
        Login method. Obtains cookie used in other API calls.

        :param host: (optional) Grid Master host name or IP address (default: 127.0.0.1).
        :param api_version: (optional) API version (default: v2.7).
        :param login: (optional) User login (default: 'admin').
        :param pwd: (optional) User password (default: 'infoblox').
        :return: ibResponse object.
        """
        self.url = "https://" + host + "/wapi/" + api_version + "/"
        login_res = requests.get(self.url + "grid", verify=self.verify_cert, auth=(login, pwd))
        self.cookie = login_res.cookies
        return ibResponse(login_res.ok, login_res.status_code, login_res.text, login_res)


    def logout(self):
        """
        Logout method. Invalidate cookie.

        :return: ibResponse object.
        """
        # If cookie exists, invalidate it. Otherwise raise an exception.
        if self.cookie.keys():
            logout_res = requests.post(self.url + "logout", verify=self.verify_cert, cookies=self.cookie)
            self.cookie = logout_res.cookies
            return ibResponse(logout_res.ok, logout_res.status_code, raw=logout_res)
        else:
            raise ibException('Session not established')


    def api_call(self, method='get', resource='grid', content_type='json', headers={}, params={}, data=''):
        """
        Generic API call.

        :param method: (optional) HTTP method: GET (default), POST, PUT, DELETE.
        :param resource: (optional) Resource to be accessed (default: 'grid').
        :param content_type: (optional) Content type of sent data: JSON (default), XML. Only for PUT and POST requests.
        :param headers: (optional) User defined HTTP headers (empty by default).
        :param params: (optional) Arguments passed together with the request (no arguments by default).
        :param data: (optional) Data sent within the request (no data by default).
        :return: ibResponse object.
        """

        # Translate HTTP methods into specific 'requests' module functions.
        methods_dict = {
            'get': requests.get,
            'post': requests.post,
            'put': requests.put,
            'delete': requests.delete
        }

        # Translate content type options into proper headers.
        content_type_dict = {
            'json': {'Content-type': 'application/json'},
            'xml': {'Content-type': 'application/xml'}
        }

        # Join content type headers with manually defined headers.
        headers.update(content_type_dict[content_type.lower()])

        # If cookie exists, make API call. Otherwise raise an exception.
        # Update cookie if a new one is returned (to extend session).
        if self.cookie.keys():
            apicall_res = methods_dict[method.lower()](self.url + resource, verify=self.verify_cert, headers=headers, params=params, data=data, cookies=self.cookie)
            if apicall_res.cookies.keys():
                self.cookie = apicall_res.cookies
            return ibResponse(apicall_res.ok, apicall_res.status_code, apicall_res.text, apicall_res)
        else:
            raise ibException('Session not established')
