class ibException(Exception):
    """
    Custom exceptions class.
    """

    def __init__(self, error_msg):
        """
        Contructor.
        """
        self.error_msg = error_msg

    def __str__(self):
        """
        Print object.
        """
        return self.error_msg

    def __repr__(self):
        """
        Reproduce object.
        """
        return repr(self.error_msg)
