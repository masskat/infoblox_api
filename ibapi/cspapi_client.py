import requests
from .ibapi_response import ibResponse
from .ibapi_exceptions import ibException


class CSPclient:
    """
    CSP API client class.
    """

    def __init__(self, api_key):
        """
        Constructor.

        :param api_key: API key.
        """
        self.api_key = api_key
        self.csp_host = 'https://csp.infoblox.com'
        self.base_url = {
            'ddi': '/api/ddi/v1',
            'thread': '/api/atcfw/v1',
            'anycast': '/api/anycast/v1',
            'zero': '/host-activation/v1',
            'onprem': '/api/host_app/v1',
            'dc': '/api/cdc-flow/v1',
            'upgrade': '/api/upgrade_policy/v2',
            'boot': '/api/bootstrap-app/v1',
            'auth': '/api/authn/v1',
            'endpoint': '/api/atcep/v1',
            'dfp': '/api/atcdfp/v1',
            'lad': '/api/atclad/v1'
        }

    def api_call(self, method='get', api_type='ddi', resource='', params={}, data=''):
        """
        Generic API call.

        URLs used in API calls are constructed in the following way:
            https://csp.infoblox.com  +  /api/ddi/v1  +  /ipam/subnet  +  ?_fields=<string>&_filter=<string>
            Host                         Base URL        Resource         Parameters

        :param method: (optional) HTTP method: GET (default), POST, PUT, DELETE.
        :param api_type: (optional) API type (base URL from API documentation). Possible values:
                         - 'ddi' (default): BloxOne DDI,
                         - 'thread': BloxOne Threat Defense -> BloxOne Threat Defense Cloud,
                         - 'anycast': BloxOne Cloud -> On-Prem Anycast Configuration Manager,
                         - 'zero': BloxOne Cloud -> Zero Touch Provisioning,
                         - 'onprem': BloxOne Cloud -> On-Prem Hosts,
                         - 'dc': BloxOne Cloud -> Data Connector,
                         - 'upgrade': BloxOne Cloud -> Schedule Software Updates,
                         - 'boot': BloxOne Cloud -> Bootstrap App,
                         - 'auth': BloxOne Cloud -> Access Authentication,
                         - 'endpoint': BloxOne Threat Defense -> BloxOne Endpoint,
                         - 'dfp': BloxOne Threat Defense -> DNS Forwarding Proxy,
                         - 'lad': BloxOne Threat Defense -> BloxOne Lookalike Domains
        :param resource: (optional) Specific resource, e.g. '/ipam/subnet'.
        :param params: (optional) Additional parameters, e.g. '_fields' or '_filter'
        :param data: (optional) JSON body used e.g in POST API calls.
        :return: ibResponse object.
        """

        # Translate HTTP methods into specific 'requests' module functions.
        methods_dict = {
            'get': requests.get,
            'post': requests.post,
            'put': requests.put,
            'delete': requests.delete,
            'patch': requests.patch
        }

        # Add HTTP headers.
        headers = {}
        headers['Authorization'] = 'Token ' + self.api_key
        if method == 'post':
            headers['Content-type'] = 'application/json'

        # API call.
        apicall_res =  methods_dict[method.lower()](self.csp_host + self.base_url[api_type] + resource, headers=headers, params=params, data=data)

        # Return ibResponse object.
        return ibResponse(apicall_res.ok, apicall_res.status_code, apicall_res.text, apicall_res)
