import json

class ibResponse:
    """
    API response class. By default returns JSON/XML/HTML response.
    """

    def __init__(self, success, status_code, data='', raw=''):
        """
        Constructor.

        Creates an object with following attributes:
            - success: True/False.
            - status_code: HTTP status code (e.g. 200).
            - data: Raw response (JSON - default, XML or HTML format). Use json() method to represent JSON data as a dictionary.
            - raw: Original 'requests' module response object.
        """
        self.success = success
        self.status_code = status_code
        self.data = data
        self.raw = raw

    def __str__(self):
        """
        Print object (response data) in human readable format.
        """
        # If response data is in JSON format, dump it with indent=4 for readability. Otherwise print original data.
        try:
            return json.dumps(self.json(), indent=4)
        except json.decoder.JSONDecodeError:
            return self.data


    def __repr__(self):
        """
        Reproduce object (response data) in human readable format.
        """
        # If response data is in JSON format, dump it with indent=4 for readability. Otherwise print original data.
        try:
            return json.dumps(self.json(), indent=4)
        except json.decoder.JSONDecodeError:
            return self.data


    def json(self):
        """
        Return JSON data as dictionary.
        """
        return json.loads(self.data)
