import setuptools
from setuptools import setup

setup(
    name="ibapi",
    version="1.1.1",
    author="masskat",
    author_email="masskat@example.com",
    description="Infoblox NIOS/CSP API library",
    long_description="Python 3 NIOS/CSP Infoblox API library.",
    long_description_content_type="text",
    url="https://bitbucket.org/masskat/infoblox_api.git",
    packages=setuptools.find_packages(),
    install_requires=['requests', 'ndict'],
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: Apache License",
        "Operating System :: OS Independent",
    ],
    python_requires='>=3',
)
