#
# network-batch.py
#
# Script for batch creating of next available networks within specified containters.
# Information about containers, network masks and comment prefixes are taken from a CSV file passed to the script as a argument.
#
# Usage:
#   ./metwork-bach.py containers.csv
#
# Example:
#   > .\network-batch.py .\containers.csv
#
#   Grid Master name or IP address: 192.168.38.25
#   Username: admin
#   Password:
#   Comment: test comment
#
#   Added subnet 10.0.18.0/24
#   Added subnet 192.168.4.0/24
#
# CSV file format:
#   container,subnet_mask,prefix
#   <container where a new network should be created>,<subnet mask of the new network>,<prefix added to the comment>
#
# Example:
#   container,subnet_mask,prefix
#   10.0.0.0/8,24,Test1
#   192.168.0.0/16,24,Test2
#

import csv
import json
import sys
import getpass
from ibapi import NIOSclient


# User input.
print('')
host = input('Grid Master name or IP address: ')
login = input('Username: ')
pwd = getpass.getpass()
comment = input('Comment: ')
print('')

# Other variables.
api_ver = 'v2.10.5'     # API version.
file = sys.argv[1]      # File name as a script argument.

# Disable certificate warnings
NIOSclient.disable_warnings()

# Create NIOSclient object and login to a Grid Master.
apiclient = NIOSclient(verify_cert=False)
res_login = apiclient.login(host=host, api_version=api_ver, login=login, pwd=pwd)

# Login successful.
if res_login.success:

    # Open and process CSV file.
    with open(file, mode='r', newline='') as in_csv:

        csv = csv.reader(in_csv)

        # Process lines in the CSV file (except headers).
        next(csv, None)
        for csv_line in csv:

            # Input JSON data.
            input_data = {
                'network': 'func:nextavailablenetwork:' + csv_line[0] + ',default,' + csv_line[1],
                'network_view': 'default',
                'comment': csv_line[2] + ' - ' + comment
            }

            # API call (HTTP POST).
            res_addnet = apiclient.api_call(method='post', resource='network', params={'_return_fields+': 'network'}, data=json.dumps(input_data))

            # Success - add information about added network.
            # Failure - print response from the GM (error data).
            if res_addnet.success:
                print('Added subnet ' + res_addnet.json()['network'])
            else:
                print('Failed to add a network to the container ' + csv_line[0] + '. Response from the Grid Master:')
                print('')
                print(res_addnet.data)
                print('')

    # Logout.
    apiclient.logout()
    print('')

# Login failed - print response from the GM.
else:
    print('Login failed! Response from the Grid Master:')
    print('')
    print(res_login.data)
    print('')
