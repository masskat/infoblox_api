#
# dhcp-import.py
#
# Import DHCP configuration (networks, ranges, fixed/reserved addresses) from NIOS CSV files to BloxOne DDI.
# Input CSV files and other settings are defined directly in the script as variables.
# Results are printed to standard output. Detailed information is logged to a file ('log_file' variable).
#
# For CSV file format see CSPimport class documentation.
#
# Usage:
#   ./dhcp-import.py
#
# Example:
#
#   nets-test.csv
#     header-network;address*;netmask*;comment;dhcp_members;domain_name;domain_name_servers;lease_time;routers;OPTION-4;OPTION-42;OPTION-66;OPTION-67
#     network;192.168.9.0;255.255.255.0;Vlan 1;infoblox.localdomain;mh.local;192.168.9.5,192.168.9.4;345600;192.168.9.1;;;;
#     network;192.168.12.0;255.255.255.0;Vlan 2;infoblox.localdomain;;10.50.50.37,10.50.50.38;345600;192.168.12.1;;;;
#     network;192.168.52.0;255.255.255.0;Vlan 3;infoblox.localdomain;mh.local;10.50.50.37,10.50.50.38;691200;192.168.52.1;192.168.52.1;;;
#
#   ranges-test.csv
#     header-dhcprange;end_address*;start_address*;exclusion_ranges;comment
#     dhcprange;192.168.9.200;192.168.9.150;;
#     dhcprange;192.168.12.254;192.168.12.10;;
#     dhcprange;192.168.52.254;192.168.52.10;192.168.52.11-192.168.52.41,192.168.52.81-192.168.52.240;
#
#   fixedip-test.csv
#     header-fixedaddress;ip_address*;mac_address;comment;name;match_option
#     fixedaddress;192.168.9.154;00:11:0a:f9:94:6b;;host1.mh.local;MAC_ADDRESS
#     fixedaddress;192.168.9.155;00:12:3f:55:50:c9;Test;host2.mh.local;MAC_ADDRESS
#     fixedaddress;192.168.9.156;5c:b9:01:12:04:d6;;host3.mh.local;MAC_ADDRESS
#     fixedaddress;192.168.52.10;00:25:64:96:3c:ae;;host4.mh.local;MAC_ADDRESS
#     fixedaddress;192.168.52.43;00:90:e8:86:82:f7;;host5.mh.local;MAC_ADDRESS
#     fixedaddress;192.168.52.9;00:00:00:00:00:00;;host6.mh.local;RESERVED
#
#   ./dhcp-import.py
#   2021-06-20 13:15:31 - Successfully added network 192.168.9.0/255.255.255.0
#   2021-06-20 13:15:32 - Successfully added network 192.168.12.0/255.255.255.0
#   2021-06-20 13:15:33 - Successfully added network 192.168.52.0/255.255.255.0
#   2021-06-20 13:15:38 - Successfully added dhcprange 192.168.9.150-192.168.9.200
#   2021-06-20 13:15:39 - Successfully added dhcprange 192.168.12.10-192.168.12.254
#   2021-06-20 13:15:40 - Successfully added dhcprange 192.168.52.10-192.168.52.254
#   2021-06-20 13:15:46 - Successfully added fixedaddress 192.168.9.154
#   2021-06-20 13:15:47 - Successfully added fixedaddress 192.168.9.155
#   2021-06-20 13:15:48 - Successfully added fixedaddress 192.168.9.156
#   2021-06-20 13:15:48 - Successfully added fixedaddress 192.168.52.10
#   2021-06-20 13:15:49 - Successfully added fixedaddress 192.168.52.43
#   2021-06-20 13:15:51 - Successfully added fixedaddress 192.168.52.9
#


import csv
import json
import datetime
import logging
from ibapi import CSPclient
from ibapi import CSPimport


#### Edit script parameters here ########################################

# CSV files (leave empty if specific CSV file is not needed):
csv_networks = 'nets-test.csv'
csv_ranges = 'ranges-test.csv'
csv_fixedips = 'fixedip-test.csv'

# Log file:
log_file = 'dhcp-import.log'

# API key:
api_key = '939d.........................76c529'

# IP space name:
space_name = 'MH-IP-Space'

# (OPTIONAL) Destination host name; leave empty if not used:
host_name = ''

# (OPTIONAL) Destination HA group; leave empty if not used:
ha_group = 'STG-DHCP-HA'

#########################################################################


# Create import session.
csp_import = CSPimport(CSPclient(api_key))

# Get common data from CSP.
options_def = csp_import.option_dict()              # Options definitions.
space_id = csp_import.ip_space_id(space_name)       # Option IP space id.
if host_name:
    host_id = csp_import.dhcp_host_id(host_name)    # Host id (if host name is defined).
elif ha_group:
    host_id = csp_import.ha_group_id(ha_group)      # Host id (if HA group is defined).
else:
    host_id = ''

# Start logging to a file.
logging.basicConfig(filename=log_file, level=logging.DEBUG, format='')
logging.debug('')
logging.debug('------------------------------------------------------------------------------------------------')
logging.debug(datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S") + ' - Starting script')


# Main program loop.
# Process networks, ranges and fixed-ips CSV files.
for file_name in (csv_networks, csv_ranges, csv_fixedips):

    #  Check if file name is defined.
    if file_name:

        # Open and read CSV file.
        with open(file_name, newline='') as csv_file:

            # Read CSV file. Each line is mapped to a dict.
            csv_reader = csv.DictReader(csv_file, delimiter=';')

            # Process a single line from the CSV file.
            for row_dict in csv_reader:

                # Logging (file).
                logging.debug('')
                logging.debug('------------------------------------------------------------------------------------------------')
                logging.debug(datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S") + ' - Adding new item from ' + file_name + ' file:')
                logging.debug(json.dumps(row_dict, indent=4))

                # Verify current line from the CSV file.
                row_check = csp_import.check_csv(row_dict)

                # Line is OK, proceed.
                if row_check['ok']:

                    # Execute an import method based on the header type.
                    if row_check['header_type'] == 'header-network':
                        res = csp_import.post_network_csv(row_dict, space_id, options_dict=options_def, dst_id=host_id)
                    elif row_check['header_type'] == 'header-dhcprange':
                        res = csp_import.post_range_csv(row_dict, space_id)
                    elif row_check['header_type'] == 'header-fixedaddress':
                        res = csp_import.post_fixedip_csv(row_dict, space_id)

                    # Logging (stdout/file) on success or failure.
                    if res.success:
                        msg = datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S") + ' - Successfully added ' + row_check['type'] + ' ' + row_check['item_id']
                        print(msg)
                        logging.debug(msg + '. Response from CSP:')
                        logging.debug(json.dumps(res.json(), indent=4))
                    else:
                        msg = datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S") + ' - Failed to add ' + row_check['type'] + ' ' + row_check['item_id']
                        print(msg)
                        logging.debug(msg + '. Response from CSP:')
                        logging.debug(res.data)

                # Content not recognized. Log (stdout/file) and skip.
                else:
                    msg = datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S") + ' - Item from ' + file_name + ' file not recognized - skipping.'
                    print(msg)
                    logging.debug(msg)
