#
# dns-import.py
#
# Import DNS records from NIOS CSV files to BloxOne DDI.
# Input CSV files should be added as script cmd parameters; other settings are defined directly in the script as variables.
# Results are printed to standard output. Detailed information is logged to a file ('log_file' variable).
#
# For CSV file format see CSPimport class documentation.
#
# Usage:
#   ./dns-import.py <csv_file1> <csv_file2> <csv_file2> ...
#
# Example:
#
#   a.csv
#     Header-ARecord;fqdn*;address*;ttl;comment
#     ARecord;raz.lab.local;192.168.38.12;;Test 1
#     ARecord;lab.local;192.168.38.11;600;Test 2
#
#   cname.csv
#     Header-CnameRecord;fqdn*;canonical_name*;ttl
#     CnameRecord;test1.lab.local;test.lab.local;28800
#
#   mx.csv
#     Header-MxRecord;fqdn*;mx*;priority*;ttl
#     MxRecord;mail.lab.local;test.lab.local;10;28800
#
#   ptr.csv
#     Header-PtrRecord;fqdn*;dname*;ttl
#     PtrRecord;20.10.168.192.in-addr.arpa;rev20.lab.local;28800
#
#   srv.csv
#     Header-SrvRecord;fqdn*;priority*;weight*;port*;target*;ttl
#     SrvRecord;_kerberos._tcp.lab.local;0;100;88;win2016-dc2.lab.local;600
#     SrvRecord;_kerberos._tcp.lab.local;0;100;88;win2016-dc1.lab.local;600
#
#   ./dns-import.py a.csv cname.csv mx.csv ptr.csv srv.csv
#   2021-11-29 15:33:04 - Successfully added ARecord raz.lab.local
#   2021-11-29 15:33:05 - Successfully added ARecord lab.local
#   2021-11-29 15:33:05 - Successfully added CnameRecord test1.lab.local
#   2021-11-29 15:33:06 - Successfully added MxRecord mail.lab.local
#   2021-11-29 15:33:06 - Successfully added PtrRecord 20.10.168.192.in-addr.arpa
#   2021-11-29 15:33:07 - Successfully added SrvRecord _kerberos._tcp.lab.local
#   2021-11-29 15:33:07 - Successfully added SrvRecord _kerberos._tcp.lab.local
#


import csv
import json
import datetime
import logging
import sys
from ibapi import CSPclient
from ibapi import CSPimport


#### Edit script parameters here ########################################

# API key:
api_key = '7e3e........................b64c'

# DNS view name:
view_name = 'default'

# Log file:
log_file = 'dns-import.log'

#########################################################################


# Create import session.
csp_import = CSPimport(CSPclient(api_key))

# Get common data from CSP.
view_id = csp_import.view_id(view_name)

# Start logging to a file.
logging.basicConfig(filename=log_file, level=logging.DEBUG, format='')
logging.debug('')
logging.debug('------------------------------------------------------------------------------------------------')
logging.debug(datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S") + ' - Starting script')


# At least one CSV file must be added as a cmd param.
if len(sys.argv) > 1:

    # Process CSV files.
    for file_name in sys.argv[1:]:

        # Open and read CSV file.
        with open(file_name, newline='') as csv_file:

            # Read CSV file. Each line is mapped to a dict.
            csv_reader = csv.DictReader(csv_file, delimiter=';')

            # Process a single line from the CSV file.
            for row_dict in csv_reader:

                # Logging (file).
                logging.debug('')
                logging.debug('------------------------------------------------------------------------------------------------')
                logging.debug(datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S") + ' - Adding new item from ' + file_name + ' file:')
                logging.debug(json.dumps(row_dict, indent=4))

                # Verify current line from the CSV file.
                row_check = csp_import.check_csv(row_dict)

                # Line is OK, proceed.
                if row_check['ok']:

                    res = csp_import.post_record_csv(row_dict, view_id)

                    # Logging (stdout/file) on success or failure.
                    if res.success:
                        msg = datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S") + ' - Successfully added ' + row_check['type'] + ' ' + row_check['item_id']
                        print(msg)
                        logging.debug(msg + '. Response from CSP:')
                        logging.debug(json.dumps(res.json(), indent=4))
                    else:
                        msg = datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S") + ' - Failed to add ' + row_check['type'] + ' ' + row_check['item_id']
                        print(msg)
                        logging.debug(msg + '. Response from CSP:')
                        logging.debug(res.data)

                # Content not recognized. Log (stdout/file) and skip.
                else:
                    msg = datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S") + ' - Item from ' + file_name + ' file not recognized - skipping.'
                    print(msg)
                    logging.debug(msg)

# No CSV files defined.
else:
    print('At least one CSV file must be added as a cmd parameter. Exiting!\n')
